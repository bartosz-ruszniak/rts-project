package sensors;

import monitoring.domain.temperature.TemperatureSensorApi;

import java.util.Random;

public class MegaAwesomeTemperatureSensorHomeEdition implements TemperatureSensorApi {

    public float getCurrentReading() {
        return new Random().nextFloat() * 100;
    }
}
