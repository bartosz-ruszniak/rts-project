package sensors;

import monitoring.domain.humidity.HumiditySensorApi;

import java.util.Random;

public class SuperHumiditySensorPro implements HumiditySensorApi {

    public float getCurrentReading() {
        return new Random().nextFloat() * 100;
    }
}
