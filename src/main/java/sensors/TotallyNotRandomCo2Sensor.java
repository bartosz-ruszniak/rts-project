package sensors;

import monitoring.domain.co2.Co2SensorApi;
import monitoring.domain.humidity.HumiditySensorApi;

import java.util.Random;

public class TotallyNotRandomCo2Sensor implements Co2SensorApi {

    public float getCurrentReading() {
        return new Random().nextFloat() * 100;
    }
}
