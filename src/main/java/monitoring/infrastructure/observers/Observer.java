package monitoring.infrastructure.observers;

public interface Observer {

    void update(ObserverDto data);
}
