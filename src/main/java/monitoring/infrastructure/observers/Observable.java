package monitoring.infrastructure.observers;

import java.util.LinkedList;
import java.util.List;

public abstract class Observable {

    private List<Observer> observers = new LinkedList<>();

    public void addObserver(Observer observer){
        observers.add(observer);
    }

    protected void notifyObservers(ObserverDto data){
        for (Observer observer : observers) {
            observer.update(data);
        }
    }
}
