package monitoring.infrastructure.listeners;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ListenerSubject {

    private List<Listener> listeners = new LinkedList<>();
    private ExecutorService executor = Executors.newFixedThreadPool(3);
    private List<Future<?>> futures = new ArrayList<>();

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    protected void notifyListeners() {

        if (areAllPreviousTasksDone()) {
            System.out.println("\n  || Next Program call\n");
            futures.clear();
        } else {
            throw new RuntimeException("Not All previous tasks are done. Our RTS did not meet the time constrains, " +
                    "this results in an error. Shouting down");
        }

        for (Listener listener : listeners) {
            Future<?> submit = executor.submit(listener::update);
            futures.add(submit);
        }
    }

    private boolean areAllPreviousTasksDone() {
        return futures
                .stream()
                .map(Future::isDone)
                .reduce(true, (a, b) -> a && b);
    }
}
