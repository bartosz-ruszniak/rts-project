package monitoring.infrastructure.listeners;

public interface Listener {
    void update();
}
