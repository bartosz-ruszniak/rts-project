package monitoring.domain.temperature.adjuster;

class TemperatureAdjusterStandByState extends TemperatureAdjusterState {

    @Override
    void decreaseTemperature(TemperatureAdjuster adjuster) {
        adjuster.getAcApi().decreaseTemperature();
        adjuster.setDecreaseState();
    }

    @Override
    void increaseTemperature(TemperatureAdjuster adjuster) {
        adjuster.getAcApi().increaseTemperature();
        adjuster.setIncreaseState();
    }
}
