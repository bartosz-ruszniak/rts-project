package monitoring.domain.temperature.adjuster;

class TemperatureAdjusterIncreasingState extends TemperatureAdjusterState {

    @Override
    void decreaseTemperature(TemperatureAdjuster adjuster) {
        adjuster.getAcApi().decreaseTemperature();
        adjuster.setDecreaseState();
    }

    @Override
    void increaseTemperature(TemperatureAdjuster adjuster) {
        System.out.println("[##] Temperature is increasing - do nothing [##]");
    }
}
