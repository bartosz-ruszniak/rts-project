package monitoring.domain.temperature.adjuster;

abstract class TemperatureAdjusterState {

    abstract void decreaseTemperature(TemperatureAdjuster adjuster);

    abstract void increaseTemperature(TemperatureAdjuster adjuster);
}
