package monitoring.domain.temperature.adjuster;

import monitoring.domain.AcApi;
import monitoring.domain.temperature.TemperatureObserverDto;
import monitoring.infrastructure.observers.Observer;
import monitoring.infrastructure.observers.ObserverDto;

public class TemperatureAdjuster implements Observer {

    private static final TemperatureAdjusterStandByState standBy = new TemperatureAdjusterStandByState();
    private static final TemperatureAdjusterDecreasingState decrease = new TemperatureAdjusterDecreasingState();
    private static final TemperatureAdjusterIncreasingState increase = new TemperatureAdjusterIncreasingState();
    private TemperatureAdjusterState state = standBy;

    private final AcApi acApi;
    private float desiredTemperature;

    public TemperatureAdjuster(AcApi acApi, float desiredTemperature) {
        this.acApi = acApi;
        this.desiredTemperature = desiredTemperature;
    }

    public void update(ObserverDto observerDto) {
        float reading = ((TemperatureObserverDto) observerDto).getTemperature();
        System.out.println("Last Recorded Temperature: " + reading);

        if (reading > desiredTemperature) {
            state.decreaseTemperature(this);
        } else if (reading < desiredTemperature) {
            state.increaseTemperature(this);
        } else {
            setStandByState();
        }
    }

    AcApi getAcApi() {
        return acApi;
    }

    void setDecreaseState() {
        state = decrease;
    }

    void setIncreaseState() {
        state = increase;
    }

    private void setStandByState() {
        state = standBy;
    }
}
