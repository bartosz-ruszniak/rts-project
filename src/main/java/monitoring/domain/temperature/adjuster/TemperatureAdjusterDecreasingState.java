package monitoring.domain.temperature.adjuster;

class TemperatureAdjusterDecreasingState extends TemperatureAdjusterState {

    @Override
    void decreaseTemperature(TemperatureAdjuster adjuster) {
        System.out.println("[##] Temperature is decreasing - do nothing [##]");
    }

    @Override
    void increaseTemperature(TemperatureAdjuster adjuster) {
        adjuster.getAcApi().increaseTemperature();
        adjuster.setIncreaseState();
    }
}
