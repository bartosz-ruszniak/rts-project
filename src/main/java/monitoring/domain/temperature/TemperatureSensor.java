package monitoring.domain.temperature;

import monitoring.infrastructure.listeners.Listener;
import monitoring.infrastructure.observers.Observable;

public class TemperatureSensor extends Observable implements Listener {

    private TemperatureSensorApi sensor;

    public TemperatureSensor(TemperatureSensorApi sensor) {
        this.sensor = sensor;
    }

    public void update() {
        float currentReading = sensor.getCurrentReading();
        notifyObservers(new TemperatureObserverDto(currentReading));
    }
}
