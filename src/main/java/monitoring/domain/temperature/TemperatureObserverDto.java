package monitoring.domain.temperature;

import lombok.Value;
import monitoring.infrastructure.observers.ObserverDto;

@Value
public class TemperatureObserverDto implements ObserverDto {
    private float temperature;
}
