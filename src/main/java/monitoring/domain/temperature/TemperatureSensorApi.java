package monitoring.domain.temperature;

public interface TemperatureSensorApi {
    float getCurrentReading();
}
