package monitoring.domain.co2;

import lombok.AllArgsConstructor;
import monitoring.infrastructure.listeners.Listener;
import monitoring.infrastructure.observers.Observable;

@AllArgsConstructor
public class Co2Sensor extends Observable implements Listener {

    private final Co2SensorApi sensor;

    public void update() {
        float currentReading = sensor.getCurrentReading();
        notifyObservers(new Co2ObserverDto(currentReading));
    }
}
