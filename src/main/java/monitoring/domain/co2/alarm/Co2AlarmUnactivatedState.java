package monitoring.domain.co2.alarm;

class Co2AlarmUnactivatedState extends Co2AlarmState {

    @Override
    void turnOn(Co2Alarm alarm) {
        alarm.getAcApi().turnOnCo2Alarm();
        alarm.setActivatedState();
    }

    @Override
    void turnOff(Co2Alarm alarm) {
    }
}
