package monitoring.domain.co2.alarm;

import monitoring.domain.AcApi;
import monitoring.domain.co2.Co2ObserverDto;
import monitoring.infrastructure.observers.Observer;
import monitoring.infrastructure.observers.ObserverDto;

public class Co2Alarm implements Observer {

    private static final Co2AlarmActivatedState on = new Co2AlarmActivatedState();
    private static final Co2AlarmUnactivatedState off = new Co2AlarmUnactivatedState();
    private final AcApi acApi;
    private Co2AlarmState state = off;
    private float alarmThreshold;

    public Co2Alarm(AcApi acApi, float alarmThreshold) {
        this.acApi = acApi;
        this.alarmThreshold = alarmThreshold;
    }

    public void update(ObserverDto observerDto) {
        float reading = ((Co2ObserverDto) observerDto).getCo2Level();

        if (reading >= alarmThreshold) {
            state.turnOn(this);
        } else if (reading < alarmThreshold) {
            state.turnOff(this);
        }
    }

    AcApi getAcApi() {
        return acApi;
    }

    void setActivatedState() {
        state = on;
    }

    void setUnactivatedState() {
        state = off;
    }
}
