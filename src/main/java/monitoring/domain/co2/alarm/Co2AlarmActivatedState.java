package monitoring.domain.co2.alarm;

class Co2AlarmActivatedState extends Co2AlarmState {

    @Override
    void turnOn(Co2Alarm alarm) {
        System.out.println("[!!!##!!!] CO2 ALARM IS ACTIVE - LET IT RING [!!!##!!!]");
    }

    @Override
    void turnOff(Co2Alarm alarm) {
        alarm.getAcApi().turnOffCo2Alarm();
        alarm.setUnactivatedState();
    }
}
