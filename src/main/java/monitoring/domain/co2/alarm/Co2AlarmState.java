package monitoring.domain.co2.alarm;

abstract class Co2AlarmState {

    abstract void turnOn(Co2Alarm alarm);

    abstract void turnOff(Co2Alarm alarm);
}
