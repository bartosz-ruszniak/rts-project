package monitoring.domain.co2;

public interface Co2SensorApi {

    float getCurrentReading();
}
