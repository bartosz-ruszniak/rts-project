package monitoring.domain.co2;

import lombok.Value;
import monitoring.infrastructure.observers.ObserverDto;

@Value
public class Co2ObserverDto implements ObserverDto {
    private float co2Level;
}
