package monitoring.domain.co2.adjuster;

import monitoring.domain.AcApi;
import monitoring.domain.co2.Co2ObserverDto;
import monitoring.infrastructure.observers.Observer;
import monitoring.infrastructure.observers.ObserverDto;

public class Co2Adjuster implements Observer {

    private static final Co2AdjusterStandByState standBy = new Co2AdjusterStandByState();
    private static final Co2AdjusterDecreasingState decrease = new Co2AdjusterDecreasingState();
    private static final Co2AdjusterIncreasingState increase = new Co2AdjusterIncreasingState();
    private Co2AdjusterState state = standBy;

    private final AcApi acApi;
    private float desiredCo2;

    public Co2Adjuster(AcApi acApi, float desiredCo2) {
        this.acApi = acApi;
        this.desiredCo2 = desiredCo2;
    }

    public void update(ObserverDto observerDto) {
        float reading = ((Co2ObserverDto) observerDto).getCo2Level();
        System.out.println("Last Recorded Co2: " + reading);

        if (reading > desiredCo2) {
            state.decreaseCo2(this);
        } else if (reading < desiredCo2) {
            state.increaseCo2(this);
        } else {
            state = standBy;
        }
    }

    AcApi getAcApi() {
        return acApi;
    }

    void setDecreaseState() {
        state = decrease;
    }

    void setIncreaseState() {
        state = increase;
    }
}
