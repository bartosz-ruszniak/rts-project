package monitoring.domain.co2.adjuster;

abstract class Co2AdjusterState {

    abstract void decreaseCo2(Co2Adjuster adjuster);

    abstract void increaseCo2(Co2Adjuster adjuster);
}
