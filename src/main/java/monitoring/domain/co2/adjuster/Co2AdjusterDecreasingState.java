package monitoring.domain.co2.adjuster;

class Co2AdjusterDecreasingState extends Co2AdjusterState {

    @Override
    void decreaseCo2(Co2Adjuster adjuster) {
        System.out.println("[##] Co2 is decreasing - do nothing [##]");
    }

    @Override
    void increaseCo2(Co2Adjuster adjuster) {
        adjuster.getAcApi().increaseCo2();
        adjuster.setIncreaseState();
    }
}
