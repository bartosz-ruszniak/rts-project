package monitoring.domain.co2.adjuster;

class Co2AdjusterStandByState extends Co2AdjusterState {

    @Override
    void decreaseCo2(Co2Adjuster adjuster) {
        adjuster.getAcApi().decreaseCo2();
        adjuster.setDecreaseState();
    }

    @Override
    void increaseCo2(Co2Adjuster adjuster) {
        adjuster.getAcApi().increaseCo2();
        adjuster.setIncreaseState();
    }
}
