package monitoring.domain.humidity.adjuster;

abstract class HumidityAdjusterState {

    abstract void decreaseHumidity(HumidityAdjuster adjuster);

    abstract void increaseHumidity(HumidityAdjuster adjuster);
}
