package monitoring.domain.humidity.adjuster;

class HumidityAdjusterStandByState extends HumidityAdjusterState {

    @Override
    void decreaseHumidity(HumidityAdjuster adjuster) {
        adjuster.getAcApi().decreaseHumidity();
        adjuster.setDecreaseState();
    }

    @Override
    void increaseHumidity(HumidityAdjuster adjuster) {
        adjuster.getAcApi().increaseHumidity();
        adjuster.setIncreaseState();
    }
}
