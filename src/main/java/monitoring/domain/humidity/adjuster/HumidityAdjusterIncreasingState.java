package monitoring.domain.humidity.adjuster;

class HumidityAdjusterIncreasingState extends HumidityAdjusterState {

    @Override
    void decreaseHumidity(HumidityAdjuster adjuster) {
        adjuster.getAcApi().decreaseHumidity();
        adjuster.setDecreaseState();
    }

    @Override
    void increaseHumidity(HumidityAdjuster adjuster) {
        System.out.println("[##] Humidity is increasing - do nothing [##]");
    }
}
