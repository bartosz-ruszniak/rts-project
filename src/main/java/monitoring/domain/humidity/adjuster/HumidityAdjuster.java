package monitoring.domain.humidity.adjuster;

import monitoring.domain.AcApi;
import monitoring.domain.humidity.HumidityObserverDto;
import monitoring.infrastructure.observers.Observer;
import monitoring.infrastructure.observers.ObserverDto;

public class HumidityAdjuster implements Observer {

    private static final HumidityAdjusterStandByState standBy = new HumidityAdjusterStandByState();
    private static final HumidityAdjusterDecreasingState decrease = new HumidityAdjusterDecreasingState();
    private static final HumidityAdjusterIncreasingState increase = new HumidityAdjusterIncreasingState();
    private HumidityAdjusterState state = standBy;

    private final AcApi acApi;
    private float desiredHumidity;

    public HumidityAdjuster(AcApi acApi, float desiredHumidity) {
        this.acApi = acApi;
        this.desiredHumidity = desiredHumidity;
    }

    public void update(ObserverDto observerDto) {
        float reading = ((HumidityObserverDto) observerDto).getHumidityLevel();
        System.out.println("Last Recorded Humidity: " + reading);

        if (reading > desiredHumidity) {
            state.decreaseHumidity(this);
        } else if (reading < desiredHumidity) {
            state.increaseHumidity(this);
        } else {
            setStandByState();
        }
    }

    AcApi getAcApi() {
        return acApi;
    }

    void setDecreaseState() {
        state = decrease;
    }

    void setIncreaseState() {
        state = increase;
    }

    private void setStandByState() {
        state = standBy;
    }
}
