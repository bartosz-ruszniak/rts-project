package monitoring.domain.humidity.adjuster;

class HumidityAdjusterDecreasingState extends HumidityAdjusterState {

    @Override
    void decreaseHumidity(HumidityAdjuster adjuster) {
        System.out.println("[##] Humidity is decreasing - do nothing [##]");
    }

    @Override
    void increaseHumidity(HumidityAdjuster adjuster) {
        adjuster.getAcApi().increaseHumidity();
        adjuster.setIncreaseState();
    }
}
