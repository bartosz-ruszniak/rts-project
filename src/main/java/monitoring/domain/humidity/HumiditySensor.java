package monitoring.domain.humidity;

import monitoring.infrastructure.listeners.Listener;
import monitoring.infrastructure.observers.Observable;

public class HumiditySensor extends Observable implements Listener {

    private HumiditySensorApi sensor;

    public HumiditySensor(HumiditySensorApi sensor) {
        this.sensor = sensor;
    }

    public void update() {
        float currentReading = sensor.getCurrentReading();
        notifyObservers(new HumidityObserverDto(currentReading));
    }
}
