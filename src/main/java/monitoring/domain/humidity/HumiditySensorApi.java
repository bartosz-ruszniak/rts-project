package monitoring.domain.humidity;

public interface HumiditySensorApi {

    float getCurrentReading();
}
