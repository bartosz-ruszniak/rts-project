package monitoring.domain.humidity;

import lombok.Value;
import monitoring.infrastructure.observers.ObserverDto;

@Value
public class HumidityObserverDto implements ObserverDto {
    private float humidityLevel;
}
