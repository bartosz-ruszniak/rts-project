package monitoring.domain;

public interface AcApi {

    void decreaseTemperature();
    void increaseTemperature();
    void decreaseHumidity();
    void increaseHumidity();
    void decreaseCo2();
    void increaseCo2();

    void turnOnCo2Alarm();

    void turnOffCo2Alarm();
}
