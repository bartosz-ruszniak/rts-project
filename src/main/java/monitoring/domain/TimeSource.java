package monitoring.domain;

import monitoring.infrastructure.listeners.ListenerSubject;

import java.util.Timer;

public class TimeSource extends ListenerSubject {

    private Timer timer = new Timer();

    public void startTimer() {
        timer.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                notifyListeners();
            }
        }, 0, 1000);
    }
}
