class ConfigurationMetadata {
    static final float desiredTemperature = 50;
    static final float desiredHumidity = 50;
    static final float desiredCo2 = 50;
    static final float co2AlarmThreshold = 80;
}
