import monitoring.domain.AcApi;
import monitoring.domain.TimeSource;
import monitoring.domain.co2.Co2Sensor;
import monitoring.domain.co2.adjuster.Co2Adjuster;
import monitoring.domain.co2.alarm.Co2Alarm;
import monitoring.domain.humidity.HumiditySensor;
import monitoring.domain.humidity.adjuster.HumidityAdjuster;
import monitoring.domain.temperature.TemperatureSensor;
import monitoring.domain.temperature.adjuster.TemperatureAdjuster;
import sensors.MegaAwesomeTemperatureSensorHomeEdition;
import sensors.SuperHumiditySensorPro;
import sensors.TotallyNotRandomCo2Sensor;

public class Main {

    public static void main(String[] args) {
        AcApi acApi = getApi();

        TemperatureSensor temperatureSensor = new TemperatureSensor(new MegaAwesomeTemperatureSensorHomeEdition());
        temperatureSensor.addObserver(new TemperatureAdjuster(acApi, ConfigurationMetadata.desiredTemperature));

        HumiditySensor humiditySensor = new HumiditySensor(new SuperHumiditySensorPro());
        humiditySensor.addObserver(new HumidityAdjuster(acApi, ConfigurationMetadata.desiredHumidity));

        Co2Sensor co2Sensor = new Co2Sensor(new TotallyNotRandomCo2Sensor());
        co2Sensor.addObserver(new Co2Adjuster(acApi, ConfigurationMetadata.desiredCo2));
        co2Sensor.addObserver(new Co2Alarm(acApi, ConfigurationMetadata.co2AlarmThreshold));

        TimeSource timeSource = new TimeSource();
        timeSource.addListener(temperatureSensor);
        timeSource.addListener(humiditySensor);
        timeSource.addListener(co2Sensor);
        timeSource.startTimer();
    }

    private static AcApi getApi() {
        return new AcApi() {

            public void decreaseTemperature() {
                System.out.println("[##] Start decreasing Temperature [##]");
            }

            public void increaseTemperature() {
                System.out.println("[##] Start increasing Temperature [##]");
            }

            public void decreaseHumidity() {
                System.out.println("[##] Start decreasing Humidity [##]");
            }

            public void increaseHumidity() {
                System.out.println("[##] Start increasing Humidity [##]");
            }

            public void decreaseCo2() {
                System.out.println("[##] Start decreasing Co2 [##]");
            }

            public void increaseCo2() {
                System.out.println("[##] Start increasing Co2 [##]");
            }

            @Override
            public void turnOnCo2Alarm() {
                System.out.println("[!!!##!!!] CO2 LEVELS HIGH ALARM ALARM ALARM [!!!##!!!]");
            }

            @Override
            public void turnOffCo2Alarm() {
                System.out.println("[!!!##!!!] CO2 LEVELS ARE BACK TO NORMAL, TURNING OFF THE ALARM [!!!##!!!]");
            }
        };
    }
}
